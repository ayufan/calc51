#include <C8051F060.H>
#include <intrins.h> 
#include "lcd.h"

char changedKeys[2];
char currentKeys[2];

sbit LCD_CE = P5^1;
sbit LCD_RW = P5^0;
sbit LCD_CD = P5^2;
sbit LCD_BL = P5^3;

#define LCD_T_BASE_HIGH 0x17
#define LCD_BYTES_PER_ROW 22

void isLcdBusy() {
#pragma asm
		push	ACC
		setb	LCD_CD
		setb	LCD_RW
		mov		P7,#0FFh
	isLcdBusy_check:
		mov		R1,#10h
		djnz	R1,$
		clr		LCD_CE
		mov		R1,#10h
		djnz	R1,$
		mov		A,P7
		setb	LCD_CE
		jnb		ACC.0,isLcdBusy_check
		jnb		ACC.1,isLcdBusy_check
		mov		R1,#10h
		djnz	R1,$
		pop		ACC
#pragma endasm
}

void cmdToLcd(char v) {
		 EA = 0;
		 isLcdBusy();
		 _nop_();
		 _nop_();
		 LCD_RW = 0;
		 LCD_CD = 1;
		 P7 = v;
		 LCD_CE = 0;
#pragma asm
		 mov	R1,#10h
		 djnz	R1,$		 
#pragma endasm
		 LCD_CE = 1;
		 EA = 1;
}

void dataToLcd(char v) {
		 EA = 0;
		 isLcdBusy();
		 _nop_();
		 _nop_();
		 LCD_RW = 0;
		 LCD_CD = 0;
		 P7 = v;
		 LCD_CE = 0;
#pragma asm
		 mov	R1,#10h
		 djnz	R1,$		 
#pragma endasm
		 LCD_CE = 1;
		 EA = 1;
}

void cmdToLcd2(char cmd, char high, char low) {
 	dataToLcd(low);
	dataToLcd(high);
	cmdToLcd(cmd);
}

void cmdToLcd1(char cmd, char v) {
	dataToLcd(v);
	cmdToLcd(cmd);
}

void initLcd() {
	cmdToLcd2(0x40, LCD_T_BASE_HIGH, 0);
	cmdToLcd2(0x41, 0, LCD_BYTES_PER_ROW);
	cmdToLcd(0x80);
	setLcdCursor(0, 0);
	cmdToLcd(0x9D);
	P5MDOUT = 0x08;
	LCD_BL = 1;
}

void clearLcd() {
	short i;
	setLcdCursor(0, 0);
 	for(i = 0; i < 1280; ++i) {
		charToLcd(' ');
	}
}

void stringToLcd(const char* fmt) {
	while(*fmt) {
		charToLcd(*fmt++);
	}
}

void clearKeyboard() {
	changedKeys[0] = changedKeys[1] = 0;	
}

void readKeyboard() {
#pragma asm
	push	ACC

	mov		A,#0FFh
	mov		P3,A

	; Read Column 0 state
	mov		A,#01111111b
	mov		P3,A
	mov		R1,#8h
	djnz	R1,$
	mov		A,P3
	anl		A,#0Fh
	mov		R7,A

	; Read Column 1 state
	mov		A,#10111111b
	mov		P3,A
	mov		R1,#8h
	djnz	R1,$
	mov		A,P3
	anl		A,#0Fh

	; Concencate results from Column 0 & 1
	swap	A				
	orl		A,R7

	; Get changed state
	push	ACC
	xch		A,currentKeys
	mov		R7,A
	pop		ACC
	cpl		A
	anl		A,R7
	orl		changedKeys,A
	
	

	; Read Column 2 state
	mov		A,#11011111b
	mov		P3,A
	mov		R1,#6h
	djnz	R1,$
	mov		A,P3
	anl		A,#0Fh
	mov		R7,A

	; Read Column 3 state
	mov		A,#11101111b
	mov		P3,A
	mov		R1,#6h
	djnz	R1,$
	mov		A,P3
	anl		A,#0Fh

	; Concencate results from Column 2 & 3
	swap	A			
	orl		A,R7

	; Get changed state
	push	ACC
	xch		A,currentKeys+1
	mov		R7,A
	pop		ACC
	cpl		A
	anl		A,R7
	orl		changedKeys+1,A

	pop		ACC
	ret
#pragma endasm
}

char getKeyChar() {
 	clearKeyboard();
	while(1) {
	 	readKeyboard();
		if(changedKeys[0]) {
			if(changedKeys[0]&1)
				return '1';
			if(changedKeys[0]&2)
				return '2';
			if(changedKeys[0]&4)
				return '3';
			if(changedKeys[0]&8)
				return '4';
			if(changedKeys[0]&16)
				return '5';
			if(changedKeys[0]&32)
				return '6';
			if(changedKeys[0]&64)
				return '7';
			if(changedKeys[0]&128)
				return '8';
		}
		else if(changedKeys[1]) {
			if(changedKeys[1]&1)
				return 'a';
			if(changedKeys[1]&2)
				return 'b';
			if(changedKeys[1]&4)
				return 'c';
			if(changedKeys[1]&8)
				return 'd';
			if(changedKeys[1]&16)
				return 'e';
			if(changedKeys[1]&32)
				return 'f';
			if(changedKeys[1]&64)
				return 'g';
			if(changedKeys[1]&128)
				return 'h';
		}
	}
	return 0;
}
