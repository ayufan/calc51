#ifndef __PARSER_H__
#define __PARSER_H__

#include "common.h"

#define P_OK 0
#define P_ERR 1

extern char xdata parser_buffer[512];
extern char xdata * parser_bufferp;
extern number xdata parser_saved;
extern number xdata parser_last;

char parser();

#endif // __PARSER_H__
