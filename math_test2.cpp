#include <math.h>
#include <stdio.h>
#include "mathlib.h"

// 100 = 64 + 36 = 64 + 32 + 12 = 64(4) + 32(2) + 8(8) + 4(4)
// 60000000

int main(int argc, char* argv[]) {
	number n1, n2, n3;
	float f1 = 0;
	float f2 = 16;
	float f3;
	numberFromFloat(&n1, f1);
	numberToFloat(&n1, &f1);
	numberFromFloat(&n2, f2);
	numberSub(&n1, &n2, &n3);
	numberToFloat(&n3, &f3);
}
