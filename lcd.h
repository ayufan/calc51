#ifndef __LCD_H__
#define __LCD_H__

void cmdToLcd(char v);
void cmdToLcd1(char cmd, char v);
void cmdToLcd2(char cmd, char high, char low);

void initLcd();
void clearLcd();

void stringToLcd(const char* fmt);

#define setLcdCursor(x, y)	cmdToLcd2(0x21, x, y)
#define charToLcd(x)		cmdToLcd1(0xC0, x - 0x20)

extern char changedKeys[2];
extern char currentKeys[2];

#endif // __LCD_H__
