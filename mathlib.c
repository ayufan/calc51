#include <stdio.h>
#include <math.h>
#include "common.h"
#include "mathlib.h"

number n_a, n_b, n_n;
float n_v;

static short e;
static unsigned char ee;
static unsigned long mm;

static char isspace(char c) {
 	if(c == ' ' || c == '\t' || c == '\n' || c == '\r')
		return 1;
	return 0;
}

static char isdigit(char c) {
 	if('0' <= c && c <= '9')
		return 1;
	return 0;
}

void numberPrint() {
	register char exp = 0;
	register char exp2 = 0;

	if(n_n.ue == 0 && n_n.um == 0) {
		printf("0");
		return;
	}

	e = n_n.ue - 0x3fff;
	if(e >= 20) {
		n_a = n_n;

		n_v = 1e-3;
		numberFromFloat();
		n_b = n_n;

		while(e > 10) {
			numberMul();
			n_a = n_n;
			e -= 10;
			exp2 += 3;
		}

		n_v = 0.1;
		numberFromFloat();
		n_b = n_n;

		while(e > 2) {
			numberMul();
			n_a = n_n;
			e -= 3;
			exp2 += 1;
		}
	}
	if(e < -10) {
		n_a = n_n;

		n_v = 1e3;
		numberFromFloat();
		n_b = n_n;

		while(e < 0) {
			numberMul();
			n_a = n_n;
			e += 10;
			exp2 -= 3;
		}
	}	

	numberToFloat();
	printf("%f", n_v);
	if(exp2)
		printf("e%i", (int)exp2);
}

static void numberNormRight() {
	if((n_n.um & 0x80000000) != 0) {
		n_n.um >>= 1;
		n_n.ue++;
	}
}

static void numberNormLeft() {
	if(n_n.um & 0x3FFFFFFF) {
		while((n_n.um & 0x40000000) == 0) {
			n_n.um <<= 1;
			n_n.ue--;
		}
	}
}

char xdata* numberFromString(char xdata* ptr) {
	register char xdata* p = ptr;
	register unsigned char size = 0;
	register char exp = 0;
	register char exp2 = 0;

	while(*p) {
		if(isspace(*p))
			p++;
		else
			break;
	}
	
	if(*ptr == '-') {
	 	n_n.sign = 1;
		ptr++;
	}
	else {
		n_n.sign = 0;
		if(*ptr == '+')			
			ptr++;
	}

	ptr = p;

	n_n.um = 0;

	// parse mantisa up to 6 digits
	while(*p && size < 8) {
		if(!isdigit(*p)) {
		 	if(*p != '.') {
				if(p == ptr)
					return NULL;
				break;
			}
			exp2 = 1;
			p++;
			continue;
		}

		n_n.um *= 10;
		n_n.um += *p - '0';

		if(exp2)
			exp--;
		++size;
		++p;
	}

	// skip rest digits
	while(*p) {
	 	if(!isdigit(*p))
			break;
		p++;
	}

	// exponent?
	switch(*p) {
		case 'e':
		case 'E':
			exp2 = 0;
			p++;
			if(*p == '-') {
				++p;
				while(isdigit(*p))
					exp2 = exp2 * 10 - (*p++ - '0');
				
			}
			else {
				if(*p == '+')
					++p;
				while(isdigit(*p))
					exp2 = exp2 * 10 + (*p++ - '0');
			}
			exp += exp2;
		break;

		case 'G':
			exp += 9;
			++p;
			break;

		case 'M':
			exp += 6;
			++p;
			break;

		case 'K':
		case 'k':
			exp += 3;
			++p;
			break;

		case 'm':
			exp -= 3;
			++p;
			break;

		case 'u':
			exp -= 6;
			++p;
			break;

		case 'n':
			exp -= 9;
			++p;
			break;

		case 'p':
			exp -= 12;
			++p;
			break;
	}

	if(n_n.um == 0) {
	 	n_n.ue = 0;
		return p;
	}

	// add exponent
	n_n.ue = 0x401d;
	numberNormLeft();

#define mul_by(x)	while(exp >= x) { n_v = 1e##x; numberFromFloat(); n_b = n_n; numberMul(); exp -= x; n_a = n_n; }
#define div_by(x)	while(exp <= -x) { n_v = 1e-##x; numberFromFloat(); n_b = n_n; numberMul(); exp += x; n_a = n_n; }

	// by 8
	if(exp > 0) {
		n_a = n_n;
		mul_by(8);
		mul_by(1);
	}
	else if(exp < 0) {
		n_a = n_n;
		div_by(8);
 		div_by(1);
	}

	return p;
}

void numberFromFloat() {
	register unsigned long * vv = (unsigned long*)&n_v;
	ee = ((*vv >> 23) & 0xFF);
	mm = *vv & 0x007FFFFF;
	if(ee || mm) {
		n_n.ue = ee + 0x3F80;
		n_n.um = mm<<7;
	}
	else {
		n_n.ue = 0;
		n_n.um = 0;
		n_n.sign = 0;
		return;
	}
	n_n.um |= 0x40000000;
	n_n.sign = *vv >> 31;
}

void numberToFloat() {
	register unsigned long * vv = (unsigned long*)&n_v;
	if(n_n.um == 0 && n_n.ue == 0) {
		*vv = 0;
		return;
	}
	*vv = (n_n.um>>7) & 0x007FFFFF;
	*vv += (unsigned long)((n_n.ue - 0x3F80) & 0xFF) << 23;
	*vv += (unsigned long)n_n.sign << 31;
}

/*
void numberToFloatWithoutExp() {
	register unsigned long * vv = (unsigned long*)&n_v;
	*vv = (n_n.um>>7) & 0x007FFFFF;
	*vv += (unsigned long)0x7F << 23;
	*vv += (unsigned long)n_n.sign << 31;
}
*/

void numberFrac() {
	e = n_n.ue - 0x3FFF;

 	n_a = n_n;
	n_a.um >>= e + 30;
	n_a.ue -= e + 30;

	if(n_a.um & 0x3FFFFFFF) {
		while((n_a.um & 0x40000000) == 0) {
			n_a.um <<= 1;
			n_a.ue--;
		}
	}

	if((n_a.um & 0x80000000) != 0) {
		n_a.um >>= 1;
		n_a.ue++;
	}

	n_b = n_n;
	n_b.um &= (1 << e) - 1;

	if((n_b.um & 0x80000000) != 0) {
		n_b.um >>= 1;
		n_b.ue++;
	}

	if(n_b.um & 0x3FFFFFFF) {
		while((n_b.um & 0x40000000) == 0) {
			n_b.um <<= 1;
			n_b.ue--;
		}
	}
}

static void numberAddStub() {
	e = n_a.ue - n_b.ue;
	n_n.sign = n_a.sign;
	if(e > 0) {
		n_n.ue = n_a.ue;
		n_n.um = (n_a.um) + (n_b.um>>e);
		numberNormRight();
	}
	else if(e < 0) {
		e = -e;
		n_n.ue = n_b.ue;
		n_n.um = (n_a.um>>e) + (n_b.um);
		numberNormRight();
	}
	else {
		n_n.ue = n_a.ue + 1;
		n_n.um = (n_a.um + n_b.um) >> 1;
	}
}

static void numberSubStub() {
	e = n_a.ue - n_b.ue;
	if(e > 0) {
		n_n.sign = n_a.sign;
		n_n.ue = n_a.ue;
		n_n.um = (n_a.um) - (n_b.um>>e);
		numberNormLeft();
	}
	else if(e < 0) {
		e = -e;
		n_n.sign = n_b.sign ^ 1;
		n_n.ue = n_b.ue;
		n_n.um = (n_b.um) - (n_a.um>>e);
		numberNormLeft();
	}
	else {
		n_n.ue = n_a.ue + 1;
		if(n_a.um >= n_b.um) {
			n_n.sign = n_a.sign;
			n_n.um = (n_a.um - n_b.um) >> 1;
		}
		else {
			n_n.sign = n_b.sign;
			n_n.um = (n_b.um - n_a.um) >> 1;
		}
		if(n_n.um == 0) {
			n_n.ue = 0;
		}
	}
}

void numberAdd() {
	if(n_a.sign == n_b.sign)
		numberAddStub();
	else
		numberSubStub();
}

void numberSub() {
	if(n_a.sign == n_b.sign)
		numberSubStub();
	else
		numberAddStub();
}

unsigned long mul64(unsigned long a, unsigned long b) {
 	unsigned long h;
	unsigned long l, ma, mb;

	unsigned short ah = a >> 15;
	unsigned short al = (a & 0x3fff) << 1;
	unsigned short bh = b >> 15;
	unsigned short bl = (b & 0x3fff) << 1;

	l = al * bl;	
	h = (unsigned long)ah * bh;

	ma = (unsigned long)al * bh;
	mb = (unsigned long)ah * bl;
	ma += mb;
	
	h += ((ma < mb) << 15) + (ma>>15);
	ma <<= 15;
	l += ma;
	h += l < ma;

	return h;
}

void numberMul() {
	n_n.sign = n_a.sign ^ n_b.sign;
	n_n.ue = n_a.ue + n_b.ue - 0x3FFF;
	n_n.um = mul64(n_a.um, n_b.um);
	
	//n_n.um = (n_a.um>>15) * (n_b.um>>15);
	numberNormRight();
}

void numberDiv() {
	n_n.sign = n_a.sign ^ n_b.sign;
	n_n.ue = n_a.ue - n_b.ue + 0x3FFF + 15;
	n_n.um = n_a.um / (n_b.um>>15);
	numberNormLeft();
}

void numberSqrt() {
 	numberToFloat();
	n_v = sqrt(n_v);
	numberFromFloat();
}
