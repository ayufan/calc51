#include <stdio.h>
#include "common.h"
#include "mathlib.h"
#include "parser.h"

int main(int argc, char* argv[]) {
	n_v = 0;
	numberFromFloat();

	while(1) {
		printf("= ");
		gets(parser_buffer);

		if(parser() == P_OK) {
			numberPrint();
			printf("\n");
		}
		else {
			printf("syntax error\n");
		}
	}
	return 0;
}
