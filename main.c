#include <C8051F060.H>
#include <stdio.h>
#include <intrins.h> 
#include <float.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "common.h"
#include "mathlib.h"
#include "parser.h"
#include "lcd.h"

sfr SCON  = 0x98;
sbit TI    = SCON^1;

#ifdef MONITOR51                     
char code reserve [3] _at_ 0x23;    
#endif                      

void main (void) {
	WDTCN = 0xde;
	WDTCN = 0xad;

#ifndef MONITOR51
    SCON  = 0x50;		        /* SCON: mode 1, 8-bit UART, enable rcvr      */
    TMOD |= 0x20;               /* TMOD: timer 1, mode 2, 8-bit reload        */
    TH1   = 221;                /* TH1:  reload value for 1200 baud @ 16MHz   */
    TR1   = 1;                  /* TR1:  timer 1 run                          */
    TI    = 1;                  /* TI:   set TI to send first char of UART    */
#endif

	//initLcd();
	//clearLcd();

	n_v = 0;
	numberFromFloat();
	parser_saved = n_n;
	parser_last = n_n;

	while(1) {
		//clearLcd();
		//setLcdCursor(0, 0);

	 	printf("? ");
	
		parser_bufferp = &parser_buffer[0];
	
		// wait for \n
	nextChar:
		switch(*parser_bufferp = getchar()) {
		 	case '\b':
			   --parser_bufferp;
			   goto nextChar;
	
		   case '\n':
		   		break;
	
			default:
				++parser_bufferp;
				goto nextChar;
		}
		*parser_bufferp = 0;

		if(parser_bufferp == parser_buffer)
			continue;
	
		// parse buffer
		if(parser() == P_OK) {
			printf("= ");
			numberPrint();
			printf(" (zapisac? [1 - tak])");
	
			if(_getkey() == '1') {
			 	parser_saved = parser_last;
				printf(" - zapisano\n");
			}
			else {
				printf("\n");
			}
		}
		else {
			printf("syntax error\n");
		}
	}
}
