expression: 
	add_expression (1)
	variable '=' add_expression (2)

add_expression:
	mul_expression '+' add_expression
	mul_expression '-' add_expression
	mul_expression

mul_expression:
	value '*' mul_expression
	value '/' mul_expression
	value

value:
	float
	variable
	'(' add_expression ')'
