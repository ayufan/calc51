#include <stdio.h>
#include <string.h>
#include <float.h>
#include <ctype.h>
#include <stdlib.h>
#include "mathlib.h"
#include "common.h"
#include "parser.h"

#define OP_END 0
#define OP_ERR 1
#define OP_VAL 2
#define OP_ADD 3
#define OP_SUB 4
#define OP_MUL 5
#define OP_DIV 6
#define OP_OPEN 7
#define OP_CLOSE 8
#define OP_SQRT	9

#define P_STACK 256

static number xdata parser_stack[P_STACK];

char xdata parser_buffer[512];
char xdata * parser_bufferp;
static char parser_token;
static number parser_tokenValue;
static char parser_unget;

static number xdata * parser_value;

number xdata parser_saved;
number xdata parser_last;

void parser_ungetToken() {
	// unget previous token
	parser_unget = 1;
}

char parser_nextToken() {
	static char c;

	if(parser_bufferp == NULL)
		return OP_ERR;

	// get previous token
	if(parser_unget) {
		parser_unget = 0;
		return parser_token;
	}

	// skip whitespaces
nextChar:
	if(!*parser_bufferp)
		return parser_token = OP_END;
	c = *parser_bufferp++;
	if(c == ' ' || c == '\t' || c == '\n')
		goto nextChar;

	// get value
	if(isdigit(c)) {
		parser_bufferp = numberFromString(parser_bufferp-1);
		if(parser_bufferp == NULL)
		  	return parser_token = OP_ERR;
		parser_tokenValue = n_n;
		return parser_token = OP_VAL;
	}

	// get operator
	if(c == '+')
		return parser_token = OP_ADD;
	if(c == '-')
		return parser_token = OP_SUB;
	if(c == '*')
		return parser_token = OP_MUL;
	if(c == '/')
		return parser_token = OP_DIV;
	if(c == '(')
		return parser_token = OP_OPEN;
	if(c == ')')
		return parser_token = OP_CLOSE;
	if(c == 'M' || c == 'm') {
		parser_tokenValue = parser_saved;
		return parser_token = OP_VAL;
	}
	if(c == 'A' || c == 'a') {
		parser_tokenValue = parser_last;
		return parser_token = OP_VAL;	 	
	}
	if(c == 'q') {
	 	return parser_token = OP_SQRT;
	}

	// error
	return parser_token = OP_ERR;
}

/*
expression: 
	add_expression (1)
	variable '=' add_expression (2)

add_expression:
	mul_expression '+' add_expression
	mul_expression '-' add_expression
	mul_expression

mul_expression:
	value '*' mul_expression
	value '/' mul_expression
	value

value:
	float
	variable
	'(' add_expression ')'
*/

static char parser_add_expression();

static char parser_getValue() reentrant {
	switch(parser_nextToken()) {
			// parse value
		case OP_SUB:
			if(parser_nextToken() != OP_VAL)
				return P_ERR;
			parser_tokenValue.sign = 1;
		case OP_VAL:
			*parser_value = parser_tokenValue;
			return P_OK;

			// parse parenthesis
		case OP_OPEN:
			if(parser_add_expression())
				return P_ERR;
			if(parser_nextToken() != OP_CLOSE)
				return P_ERR;
			return P_OK;

		case OP_SQRT:
			if(parser_getValue())
				return P_ERR;
			n_n = *parser_value;
			numberSqrt();
			*parser_value = n_n;
			return P_OK;

		default:
			return P_ERR;
	}
}

static char parser_mul_expression() {
	// get first value
	if(parser_getValue())
		return P_ERR;

	// get next token
nextMulExpression:
	switch(parser_nextToken()) {
		case OP_MUL:
			// alocate next value
			if(parser_value+1 == &parser_stack[P_STACK])
				return P_ERR;

			// get next value
			++parser_value;
			if(parser_getValue())
				return P_ERR;
			--parser_value;

			// calculate value
			n_a = parser_value[0];
			n_b = parser_value[1];
			numberMul();
			parser_value[0] = n_n;
			goto nextMulExpression;

		case OP_DIV:
			// alocate next value
			if(parser_value+1 == &parser_stack[P_STACK])
				return P_ERR;

			// get next value
			++parser_value;
			if(parser_getValue())
				return P_ERR;
			--parser_value;

			// calculate value
			n_a = parser_value[0];
			n_b = parser_value[1];
			numberDiv();
			parser_value[0] = n_n;
			goto nextMulExpression;

		default:
			parser_ungetToken();
			return P_OK;
	}
}

static char parser_add_expression() {
	// get first value
	if(parser_mul_expression())
		return P_ERR;

	// get next token
nextAddExpression:
	switch(parser_nextToken()) {
		case OP_ADD:
			// alocate next value
			if(parser_value+1 == &parser_stack[P_STACK])
				return P_ERR;

			// get next value
			++parser_value;
			if(parser_mul_expression())
				return P_ERR;
			--parser_value;

			// calculate value
			n_a = parser_value[0];
			n_b = parser_value[1];
			numberAdd();
			parser_value[0] = n_n;
			goto nextAddExpression;

		case OP_SUB:
			// alocate next value
			if(parser_value+1 == &parser_stack[P_STACK])
				return P_ERR;

			// get next value
			++parser_value;
			if(parser_mul_expression())
				return P_ERR;
			--parser_value;

			// calculate value
			n_a = parser_value[0];
			n_b = parser_value[1];
			numberSub();
			parser_value[0] = n_n;
			goto nextAddExpression;

		default:
			parser_ungetToken();
			return P_OK;
	}
}

static char parser_expression() {
	return parser_add_expression();
}

char parser() {
	// reset parser state
	parser_bufferp = parser_buffer;
	parser_unget = 0;
	parser_value = &parser_stack[0];
	
	// parse expression
	if(parser_expression())
		return P_ERR;

	// any left token's?
	if(parser_nextToken() != OP_END)
		return P_ERR;

	// save value
	n_n = parser_stack[0];
	parser_last = n_n;
	return P_OK;								
}
