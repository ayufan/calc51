#ifndef _MATHLIB_H_
#define _MATHLIB_H_

#include "common.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct number_s {
	unsigned long um;
	unsigned short ue : 15;
	unsigned char sign : 1;
} number;

extern number n_a, n_b, n_n;
extern float n_v;

//! ptr -> n_n
char xdata* numberFromString(char xdata* ptr);

//! n_n -> (screen)
void numberPrint();

//! n_v -> n_n
void numberFromFloat();

//! n_n -> n_v
void numberToFloat();
void numberToFloatWithoutExp();

//! n_n = n_a + n_b
void numberAdd();

//! n_n = n_a - n_b
void numberSub();

//! n_n = n_a * n_b
void numberMul();

//! n_n = n_a / n_b
void numberDiv();

//! n_n -> n_a . n_b
void numberFrac();

//! n_n = sqrt(n_n)
void numberSqrt();

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _MATHLIB_H_
